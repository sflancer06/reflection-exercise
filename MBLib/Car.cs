﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLib
{
	public class Car
	{
		public string make { get; set; }
		public string model { get; set; }
		public int year { get; set; }

		public int weight;
		public int height;
		public int speed;
		public int length;
		public int width;

		public Car()
		{
			speed = 0;
			make = "Mercedes-Benz";
			model = "SL500 Sport";
			year = 1997;
			length = 4498;
			width = 4470;
			weight = 2123;
			height = 1288;
		}

		public int accelerate(int kmh) { speed += kmh; return speed; }
		public int decelerate(int kmh) { speed -= kmh; return speed; }
	}
}
