﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using MBLib;

namespace MB_Application_2014_04_29
{
	public class Reflector
	{
		List<MethodInfo> mi = new List<MethodInfo>();
		List<FieldInfo> fi = new List<FieldInfo>();
		List<PropertyInfo> pi = new List<PropertyInfo>();
		public string moduleName { get; set; }
		private Car car;

		public Reflector() { load(); }

		public void load()
		{
			car = new Car();
			
			Module m = car.GetType().Module;
			moduleName = m.Name;
			Type[] types = m.GetTypes();

			foreach (Type t in types)
			{
				foreach (MethodInfo info in t.GetMethods())
					mi.Add(info);
				foreach (FieldInfo info in t.GetFields())
					fi.Add(info);
				foreach (PropertyInfo info in t.GetProperties())
					pi.Add(info);
			}
		}
		public void viewMethods()
		{
			Console.Clear();
			Console.WriteLine("Printing all methods:");
			Console.WriteLine("#\tClass\tType\tName\t");
			int count = 0;
			foreach (MethodInfo m in mi)
			{
				string className = m.ReflectedType.ToString();
				className = className.Substring(className.IndexOf('.') + 1);
				string returnType = m.ReturnType.ToString();
				returnType = returnType.Substring(returnType.IndexOf('.') + 1);

				string output = "(" + (++count).ToString() + ")\t";
				output += className + '\t';
				output += returnType + '\t';
				output += m.Name;

				ParameterInfo[] pars = m.GetParameters();
				if (pars.Length == 1)
					foreach (ParameterInfo p in pars)
					{
						string temp = p.ParameterType.ToString();
						while (temp.Contains('.'))
							temp = temp.Substring(temp.IndexOf('.') + 1);
						Console.WriteLine(output + "(" + temp + " " + p.Name + ")");
					}
			}
			Console.Write("\nCall a method or press enter to go back.\nexample: foo(bar)\nInput: ");
			execute();

		}

		// Attempt to execute a specified function.
		private void execute()
		{
			string input;
			do
			{
				input = Console.ReadLine();
				try
				{
					foreach (MethodInfo m in mi)
					{
						// Look for a function that matches the user input.
						if (m.Name == input.Substring(0, input.IndexOf('(')))
						{
							int a = input.IndexOf('(') + 1;
							int b = input.IndexOf(')');
							string inValue = input.Substring(a, b - a);

							object[] inParams;
							
							try { inParams = new object[] { int.Parse(inValue), long.Parse(inValue) }; }
							catch { inParams = new object[] { inValue[0], inValue }; }

							foreach (object o in inParams)
							{
								try
								{
									var x = m.Invoke(car, new object[] { o });
									if (m.ReturnType.ToString() != "System.Void")
										Console.WriteLine(x.ToString());
									else
										Console.WriteLine("Function ran successfully, but returned void.");
									Console.Write("Input: ");
								}
								catch { }
							}
						}
					}
				}
				catch { Console.Write("Input: "); }
			} while (input.Length > 0);
		}

		public void viewProperties()
		{
			Console.Clear();
			Console.WriteLine("Printing all properties:");
			Console.WriteLine("#\tClass\tName\tType");
			int count = 0;
			foreach (PropertyInfo p in pi)
			{
				string className = p.ReflectedType.ToString();
				className = className.Substring(className.IndexOf('.') + 1);

				string output = "(" + (++count).ToString() + ")\t";
				output += className + '\t';
				output += p.Name + '\t';
				output += p.PropertyType;

				Console.WriteLine(output);
			}
			Console.WriteLine("\nType the name of the property you wish to view or press enter to go back.");

			displayProperty();
		}
		private void displayProperty()
		{
			string input;
			do
			{
				Console.Write("Input: ");
				input = Console.ReadLine();
				foreach (PropertyInfo p in pi)
					if (p.Name == input)
					{
						string output = "Value of " + p.Name + ":\t";
						var v = p.GetValue(car, null);

						Console.WriteLine("Value of " + p.Name + ":\t" + v.ToString());
					}
			} while (input.Length > 0);
		}

		public void printFields()
		{
			Console.Clear();
			Console.WriteLine("Printing all fields:");
			Console.WriteLine("#\tClass\tName\tData Type\tValue");
			int count = 0;
			foreach (FieldInfo f in fi)
			{
				string className = f.ReflectedType.ToString();
				className = className.Substring(className.IndexOf('.') + 1);

				string output = "(" + (++count).ToString() + ")\t";
				output += className + '\t';
				output += f.Name + '\t';
				output += f.FieldType.ToString() + '\t';
				output += f.GetValue(car);

				Console.WriteLine(output);
			}

			Console.Write("\nPress any key to return to the menu.");
			Console.ReadKey();
		}
	}
}