﻿/*	
	Author:	Will Maynard
	Date:	2014-04-29
	Desc:	A simple application that uses reflection
 			to provide information about a class' module.

	Write a console application in C# that uses the .net reflection 
	API to print out all fields of a module (pick any class you like).

	Allow for selection of a property field, print out its value.

	Allow for selecting and invocation of a method that requires a 
	single parameter.
*/

using System;
using System.Collections.Generic;
using MBLib;

namespace MB_Application_2014_04_29
{
	public class Program
	{
		public static void Main()
		{
			Reflector r = new Reflector();

			bool exit = false;

			while(!exit)
			{
				displayMenu(r.moduleName);
				switch (Console.ReadKey().KeyChar)
				{
					case '1':
						r.printFields();
						break;
					case '2':
						r.viewProperties();
						break;
					case '3':
						r.viewMethods();
						break;
					case '4':
						exit = true;
						break;
					default:
						break;
				}
			}
		}
		private static void displayMenu(string module)
		{
			Console.Clear();
			Console.Write(
				"Loaded module: " + module + "\n\n" +
				"What would you like to do?\n" +
				"\t(1) Print all fields from the module.\n" +
				"\t(2) View all properties in the module.\n" +
				"\t(3) Invoke a single-parameter method in the module.\n" +
				"\t(4) Exit this program.\n\n" +
				"Selection: ");
		}

	}
}